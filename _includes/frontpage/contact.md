<h2>Contact Us</h2>

<p>Follow us on Facebook for the best deals on firewood in Co. Clare.
<ul class="list-inline banner-social-buttons">
  <li><a href="https://www.facebook.com/pages/Midwestfirewoodie" class="btn btn-default btn-lg">
  <i class="fa fa-twitter fa-fw"></i><span class="network-name">Facebook</span></a>
  </li>
</ul></p>
<a href="tel:+353870546722" class="mobile-tel click-phone btn btn-default btn-lg"><i class="fa fa-phone"></i>&nbsp;Click to Call</a>
<div class="desktop-tel btn btn-default btn-lg"><i class="fa fa-phone"></i>&nbsp;Call Now: 087 054 6722</div>
