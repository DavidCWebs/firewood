<h2>Log-Splitter Hire</h2>
<p>Hire our top-quality splitter to make short work of your logpile!</p>
<p>Our splitter takes all the heavy work out of preparing your timber for burning. You'll save hours of heavy labour.</p>
<p>Click the images below to find out more about using our splitter.</p>
 <div id="grid">
<div class="thumbnail">
<h4>Using a Splitter</h4>
  <a id="modal-1" href="#modal-container-1" data-toggle="modal"><img src="img/logsplitter-opt.jpg"></a>
  <div class="modal fade" id="modal-container-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="modal-title" id="myModalLabel">
            Why Use a Log Splitter?
          </h3>
        </div>
        <div class="modal-body">
          <!--<div class="thumbnail">-->
            <img class="img-responsive" alt="log splitter" src="img/logsplitter-opt.jpg">
            <div class="caption">
              <p>If you have a lot of timber to split, there is no substitute for a good-quality log splitter.</p>
              <p>Simply load the splitter with a ring of timber and engage the ram. This pushes the wood aganst a spike, splitting the ring effortlessly.</p>
              <p>Using a splitter takes half the time required to split timber rings by axe, and is way less back-breaking!</p>
            </div>
          <!--</div>-->
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           <button type="button" class="btn btn-default" data-dismiss="modal" id="modal-2" href="#modal-container-2" data-toggle="modal">Splitter Hire Details</button>
        </div>
      </div>
    </div>
  </div>
</div><!-- End Thumbnail -->
    <div class="thumbnail">
    <h4>Splitter Hire Details</h4>
    <a id="modal-2" href="#modal-container-2" data-toggle="modal"><img src="img/splitter-opt.jpg"></a>
<div class="modal fade" id="modal-container-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title" id="myModalLabel">
          Splitter Hire
        </h3>
      </div>
      <div class="modal-body">
        <!--<div class="thumbnail">-->
          <img class="img-responsive" alt="log splitter" src="img/splitter-opt.jpg">
          <div class="caption">
            <p>
              Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
            </p>
          </div>
        <!--</div>-->
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
