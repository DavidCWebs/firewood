#Get started

* Clone & rename repo
* cd to project directory and run ```sudo npm install``` to install grunt modules
* run ```bower install```
* Change path for Font-Awesome since this will be installed by Bower

Font-Awesome path, set on bower_components/font-awesome/less/variables.less:

~~~
@fa-font-path:        "../bower_components/font-awesome/fonts";
~~~

Note that this is path is relative to the **compiled** css, which will be in /css

---

##How to render markdown in a jekyll include:

This method creates a tag called "markdown" that can be used instead of the "include" tag.

Create a file called "markdown.rb" in your sites _plugins directory. In the file include this code:

```
=begin

  Jekyll tag to include Markdown text from _includes directory preprocessing with Liquid.

  Usage:

    {% markdown <filename> %}

  Dependency:

    - kramdown

=end

module Jekyll

  class MarkdownTag < Liquid::Tag

    def initialize(tag_name, text, tokens)

      super

      @text = text.strip

    end

    require "kramdown"

    def render(context)

      tmpl = File.read File.join Dir.pwd, "_includes", @text

      site = context.registers[:site]

      tmpl = (Liquid::Template.parse tmpl).render site.site_payload

      html = Kramdown::Document.new(tmpl).to_html

    end

  end

end

Liquid::Template.register_tag('markdown', Jekyll::MarkdownTag)
```

You can now use the "markdown" tag instead of the "include" tag to bring through includes with markdown rendered. For example:

{% markdown box-1.markdown %}

Instead of:

{% include box-1.markdown %}
