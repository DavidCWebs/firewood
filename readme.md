#About this site:

This is a Bootstrap Jekyll website.

The content is brought through in a modular fashion, with the content being written as includes in
the markdown format. The Gruntfile runs Livereload alongside the other standard tasks.

---

#Get started

* Clone & rename repo
* cd to project directory and run ```sudo npm install``` to install grunt modules
* run ```bower install```
* Change path for Font-Awesome since this will be installed by Bower

Font-Awesome path, set in bower_components/font-awesome/less/variables.less:

~~~
@fa-font-path:        "../bower_components/font-awesome/fonts";
~~~

Note that this is path is relative to the **compiled** css, which will be in /css

---
